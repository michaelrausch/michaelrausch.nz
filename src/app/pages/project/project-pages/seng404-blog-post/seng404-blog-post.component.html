<app-page-header headerText="Capturing requirements workshops" class="grad-red-bg"></app-page-header>

<div class="container">
  <h1>Introduction</h1>

  <p>Requirements workshops are commonly used by project stakeholders, team representatives, and developers to build a shared understanding of software requirements. Workshops not only provide attendees with an understanding of the software requirements, they also provide context, about why the requirements are important, and how the software is intended to be used. The end product of a workshop is commonly some form of requirement speciﬁcation document. This document, however, fails to capture some of the more organic aspects of the workshop. The opinions, rationale, and ideas behind the requirements are not captured in this edited document.</p>
  <p>Requirement workshop videos have been proposed as a solution to counter these ﬂaws. Previously, however, videos have been expensive and time-consuming to produce. However, video technology has become more aﬀordable, and accessible in recent years. A study completed in 2014 considered whether or not videos could be used to capture requirement workshops. In this blog post, we'll explore the ﬁndings of this paper.</p>
    
</div>

<div class="container">
  <h1>Why do we need a better way to capture workshops?
  </h1>

  <p>Requirement workshops provide a way for key stakeholders to discuss the requirements for a software project. One of the most important recipients of these requirements, developers, often cannot be present in these workshops. There are many reasons why developers may not be present, usually, it’s because they haven’t yet been assigned to the project at the point when requirement workshops take place. Also, projects that involve public tenders strictly separate the speciﬁcation of requirements from the development of the software. Developers usually aren’t allowed to be involved in these instances. Developers can also be assigned to a project after development has started, in these cases, developers need to become familiar with the requirements of the project long after the workshop has taken place.
  </p>
  <p>Traditional documentation, although useful, only shows the end products of the workshop. Developers miss out on the discussions that lead to requirements being discovered. Documents can also be time-consuming to produce, and prone to errors. If a developer were to need clariﬁcation on a requirement, or if they required a clearer understanding of what a requirement meant to the stakeholder, they would need to directly contact the stakeholder who was present. This feedback can take a long time to get if it’s even possible to contact the desired stakeholder. Stakeholders, like developers, will change teams, change projects, and even change companies. This leads to information gaps that cannot be ﬁlled.
  </p>
    
</div>

<div class="container">
  <h1>Why use video?
  </h1>

  <p>Videos have, for a long time, been used a tool to capture rich information. A video captured of a requirement workshop could let developers observe the full proceedings of the workshop, and therefore gain a fuller understanding of the requirements developed without needing to seek clariﬁcations from the stakeholders. This can result in a richer, more concrete understanding of requirements, and the impact they have on the stakeholders. This leads to less ambiguity, and fewer mistakes when interpreting requirements.
  </p>
    
</div>

<div class="container">
  <h1>Implementing Workshop Videos
  </h1>

  <p>For workshop videos to be effective, creators need to ensure that the video is affordable to produce. To achieve this, the video equipment used should be as lightweight, and cheap as possible. This could mean using a mobile phone, or a cheap camcorder. It is important, however, to ensure that participants are able to be seen and heard clearly by the viewer. The production of the video shouldn’t require any special skills, this means that editing should be kept to a minimum. </p>
  <p>The producer also needs to consider the ethical issues arising from ﬁlming attendees. All participants need to be made aware of the ﬁlming taking place, and how the video is intended to be used. Some participants may be uncomfortable being ﬁlmed, so accommodations will need to be made for those participants while minimising the impact of this on developers watching the video. There is also the potential during the workshop for a participant to say something they would like removed from the video. </p>
</div>

<div class="container">
  <h1>What affects the quality of a workshop video?</h1>

  <p>Although no students in the study found the quality of the workshop video to be poor or bad, they still showed that there was signiﬁcant room for improvement.</p>
  <p>For a workshop to be eﬀective, it needs to be well moderated. Participants also need to be well prepared before the workshop begins. Students noted distractions, for example, a participant turning up late, and disrupting the discussion. Distractions like this make the video longer, and harder to follow, so they should be avoided and managed where possible. The students also found that the workshop started without a clear agenda, this resulted in the workshop being less organised, thereby also making it more diﬃcult for viewers to follow. An introduction to the participants, and what their roles are in the project would have given some context to their discussions. The students also noted that it was hard to follow conversations when there were multiple people talking at the same time. Better moderation could have reduced the length of the workshop, thereby making the length of the video shorter and easier for viewers to follow</p>
  <p>The students appreciated being introduced to the background of the software being developed. However, they would have favoured a more thorough, systematic introduction to how the software is going to be used, and what devices the system is going to interact with. The students would have also liked the stakeholders to more clearly portray their goals and expectations for the software. The motivation behind requirements would have been beneﬁcial in situations where the motivation isn't necessarily intuitive for developers. </p>

</div>

<div class="container">
  <h1>How should videos be used to communicate requirements?</h1>

  <p>Finally, we would like to know, from the perspective of the developers included in this study, how should videos be used to communicate requirements to developers?</p>
  <p>Firstly, if the workshop itself is poorly run, the video has little chance of being useful. It’s essential then, for the workshop to be run properly. The moderator should arrive at the workshop prepared, with an agenda of the workshop's activities. The moderator needs to stick to the agenda, and avoid allowing unnecessary, or overlapping conversations to take place. These steps will result in a shorter, easier to follow video as a result. The students advocate only allowing genuine stakeholders to participate in the workshop, this would increase their conﬁdence in the requirements being put forward. Another suggestion raised by students was to include an introduction session at the beginning of the workshop where stakeholders would introduce themselves to add context to their discussions. A closure session at the end of the workshop could be introduced so stakeholders can summarise points made during the workshop. </p> 
  <p>The students would have liked the camera crew to use multiple cameras. This would help viewers get a fuller picture of what was happening in the workshop. Multiple microphones could be used to ensure that all participants could be heard clearly. Low voice quality, and the inability to hear some participants were both complaints raised during the study. They also suggested removing disturbances, and irrelevant content in post-production. It is important, however, to be prudent not to unintentionally remove important information from the discussion. Editors could also add bookmarks, or split the video up into segments to make it easier to ﬁnd information. Although many of these improvements would make the video more useful to developers, they would also make the video more expensive to produce and would be a hindrance to some teams.</p>
  <p>The students also came up with many diﬀerent uses for the workshop video. They suggested using the video to educate developers and stakeholders, who may not have attended a requirement workshop before. The workshop video could also be used to help new team members familiarise themselves with the project. Finally, the video could be used not only as a tool to understand requirements but also as a reference tool to clarify a requirement being implemented.</p>
</div>

<div class="container">
  <h1>Conclusion</h1>

  <p>Traditionally, the outcomes of requirement workshops have been communicated to developers either in person or by using documentation. These methods, however, are not always suﬃcient for developers to gain a full understanding of the requirements or the motivation behind them. Documents can lack important detail, and are prone to errors. Direct communication with stakeholders can be time-consuming and is often not possible. Workshop videos have been proposed as a low-cost way to overcome these issues. Video has the ability to capture interactions between stakeholders and avoids cutting out information that an edited document does. This helps reduce ambiguity and uncertainty when implementing requirements in software. Videos on their own may not be suﬃcient to communicate requirements, but they show promise when complemented with documentation, and person to person interaction. Out of eighteen senior software engineering students who participated in a study, 83% said they would use video as a way to capture requirement workshops. </p>

</div>

<div class="container">
  <h1>Acknowledgements</h1>

  <p>This blog post was based on the paper "Workshop videos for requirements communication" by Samuel A. Fricker, Kurt Schneider, Farnaz Fotrousi, and Christoph Thuemmler.</p>
  <p><i>Fricker, S.A., Schneider, K., Fotrousi, F. et al. Requirements Eng (2016) 21: 521. https://doi.org/10.1007/s00766-015-0231-5 
  </i></p>
</div>

<!-- <div class="container">
  <div class="yt-container">
      <iframe src="https://www.youtube.com/embed/AEB6ibtdPZc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
  </div>
</div> -->

<div class="download-section">
  <div class="container">
    <div class="dl-header">
      <h1>Download</h1>
    </div>

    <div class="cta-button-container row">
        <div class="dl-link col-md-6">
            <a href="" class="">
              <i class="fa fa-download hvr-grow"></i>
              <div class="link-text">Download</div> 
            </a>
        </div>
  
        <div class="dl-link col-md-6">
            <a href="" class="">
              <i class="fa fa-code hvr-grow"></i>
              <div class="link-text">View Source</div> 
            </a>
        </div>
    </div>

  </div>
</div>